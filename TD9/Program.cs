﻿using System;
using System.Net;
using System.Net.Sockets;

namespace TD9
{
    class Program
    {
        static void Main(string[] args)
        {
            
                byte[] trame_NTP = new byte[48];
                byte[] reponse_NTP;
                IPEndPoint iep = new IPEndPoint(0,0);
                // Construction de la trame à envoyer au serveur NTP
                // Champ VN à 4 champ MDE à 3, donc le premier octet de
                // 0 1 2 3 4 5 6 7
                // +-+-+-+-+-+-+-+-+
                // |LI | VN | Mode |
                // +-+-+-+-+-+-+-+-+
                // +0 0|1 0 0|0 1 1+
                // Soit 2 | 3 en héxadécimal
                trame_NTP[0] = 0x23;
                try
                {
                UdpClient client = new UdpClient("ntp.u-picardie.fr",123);

                client.Send(trame_NTP, 48);
                reponse_NTP = client.Receive(ref iep);
                client.Close();

                }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }
            Array.Reverse(reponse_NTP, 40, 4);
                uint nb_sec = BitConverter.ToUInt32(reponse_NTP, 40);
                DateTime date = new DateTime(1900,1,1);
                date = date.AddSeconds(nb_sec);
                date = date.ToLocalTime();
                Console.WriteLine("Serveur : {0}", iep);
                Console.WriteLine("Reçu : {0}", date);
                Console.ReadKey();


        }
    }
}
